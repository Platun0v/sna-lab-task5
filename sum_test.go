package main

import "testing"

func TestSum(t *testing.T) {
	var a, b, want int
	a, b, want = 1, 2, 3
	if got := Sum(a, b); got != want {
		t.Errorf("Sum(%d, %d) = %d; want %d", a, b, got, want)
	}
}
