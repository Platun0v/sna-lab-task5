# syntax=docker/dockerfile:1

## Build
FROM golang:1.19 AS build

WORKDIR /app

COPY . ./

RUN go build -o sum

## Deploy
FROM alpine:latest

WORKDIR /prod

COPY --from=build /app/sum /prod/sum

ENTRYPOINT ["/prod/sum"]
